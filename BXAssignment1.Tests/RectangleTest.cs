﻿///BXAssignment1
///Braedon Xuereb 2018
///Prog2070 - Quality Assurance
///Conestoga College
///Created: Feburary 4 2018

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;

namespace BXAssignment1.Tests
{
    [TestFixture]
    public class RectangleTest
    {
        
        Rectangle testRectangle = new Rectangle();
        [TestCase]
        
        
        public void GetLength_success_WillDisplayLength()
        {
            
            testRectangle.length = 5;
            Assert.AreEqual(5, testRectangle.GetLength());
        }

        [TestCase]
        public void ChangeLength_success_WillChangeLength()
        {
            testRectangle.length = 5;
            testRectangle.newLength = 10;
            Assert.AreEqual(10, testRectangle.SetLength());
        }

        [TestCase]

        public void GetWidth_success_WillDisplayWidth()
        {
            testRectangle.width = 5;
            Assert.AreEqual(5, testRectangle.GetWidth());
        }


        [TestCase]

        public void ChangeWidth_success_WillChangeWidth()
        {
            testRectangle.width = 5;
            testRectangle.newWidth = 10;
            Assert.AreEqual(10, testRectangle.SetWidth());
        }

        [TestCase]

        public void GetPerimeter_success_WillDisplayPerimeter()
        {
            testRectangle.width = 5;
            testRectangle.length = 5;
            Assert.AreEqual(20, testRectangle.GetPerimeter());
        }

        [TestCase]

        public void GetArea_success_WillDisplayArea()
        {
            testRectangle.width = 5;
            testRectangle.length = 5;
            Assert.AreEqual(25, testRectangle.GetArea());
        }
    }

}
